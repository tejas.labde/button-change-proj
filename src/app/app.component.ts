import { Component,EventEmitter,Input,Output } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  value1=0;
  value2=0;

  changeValue1(value1:number){
    this.value1=value1;
  }
  changeValue2(value2:number){
    this.value2=value2;
  }

  title = 'button-change';
}
