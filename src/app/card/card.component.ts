import { Component, OnInit ,Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {

  value2=0;
  @Input() value1=0;
  @Output() valueEmitter1=new EventEmitter ();
 
 changeValue2():void{
  this.valueEmitter1.emit(++this.value2);
}

  constructor() { }

  ngOnInit(): void {
  }

}
