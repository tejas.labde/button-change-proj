import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-card2',
  templateUrl: './card2.component.html',
  styleUrls: ['./card2.component.css']
})
export class Card2Component implements OnInit {

  value1=0;
  @Input() value2=0;
  @Output() valueEmitter2=new EventEmitter();
 
 changeValue1():void{
  this.valueEmitter2.emit(++this.value1);
}


  constructor() { }

  ngOnInit(): void {
  }

}
